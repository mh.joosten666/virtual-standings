import csv
import pandas
import json
from pprint import pprint
from typing import List, Dict, Tuple

import pandas as pd
from pandas import DataFrame


def load_current_standings() -> DataFrame:
    return pandas.read_csv('current_standings.csv', index_col='plaats', converters={'plaats':int,
                                                                'wedstrijden':int,
                                                                'winst':int,
                                                                'gelijk':int,
                                                                'verlies':int,
                                                                'punten':int,
                                                                'goals':int,
                                                                'tegengoals':int,
                                                                'doelsaldo':int})




def load_scenario(scenario_file, team_list) -> DataFrame:
    sc = pandas.read_csv(scenario_file)
    home_contains = sc['thuis'].isin(team_list).all()
    away_contains = sc['uit'].isin(team_list).all()

    if home_contains and away_contains:
        return sc

    raise KeyError('Scenario should contain teams from the current standings')


def calculate_points(a, b):
    if a > b:
        return 3
    if a == b:
        return 1

    return 0


def calculate_new_standings(current_standings: DataFrame, scenario: DataFrame):
    for thuis, uit, voor, tegen in zip(scenario['thuis'], scenario['uit'], scenario['voor'], scenario['tegen']):
        current_standings.loc[current_standings['club'] == thuis, 'wedstrijden'] += 1
        current_standings.loc[current_standings['club'] == thuis, 'winst'] += 1 if voor > tegen else 0
        current_standings.loc[current_standings['club'] == thuis, 'gelijk'] += 1 if voor == tegen else 0
        current_standings.loc[current_standings['club'] == thuis, 'verlies'] += 1 if voor < tegen else 0
        current_standings.loc[current_standings['club'] == thuis, 'punten'] += calculate_points(voor, tegen)
        current_standings.loc[current_standings['club'] == thuis, 'goals'] += voor
        current_standings.loc[current_standings['club'] == thuis, 'tegengoals'] += tegen
        current_standings.loc[current_standings['club'] == thuis, 'doelsaldo'] += (voor - tegen)

        current_standings.loc[current_standings['club'] == uit, 'wedstrijden'] += 1
        current_standings.loc[current_standings['club'] == uit, 'winst'] += 1 if tegen > voor else 0
        current_standings.loc[current_standings['club'] == uit, 'gelijk'] += 1 if tegen == voor else 0
        current_standings.loc[current_standings['club'] == uit, 'verlies'] += 1 if tegen < voor else 0
        current_standings.loc[current_standings['club'] == uit, 'punten'] += calculate_points(tegen, voor)
        current_standings.loc[current_standings['club'] == uit, 'goals'] += tegen
        current_standings.loc[current_standings['club'] == uit, 'tegengoals'] += voor
        current_standings.loc[current_standings['club'] == uit, 'doelsaldo'] += (tegen - voor)

    sorted_values = current_standings.sort_values(['punten', 'doelsaldo', 'wedstrijden'], ascending=False).reset_index(drop=True)

    return sorted_values


def execute_scenarios(scenarios: List[Dict], end_position_team: str) -> List[Dict]:
    output_list = pd.DataFrame()
    for scenario_def in scenarios:
        file = scenario_def['file']
        desc = scenario_def['desc']
        current_standings = load_current_standings()
        scenario = load_scenario(file, current_standings['club'])
        new_standings = calculate_new_standings(current_standings, scenario)

        team_row = new_standings.loc[new_standings['club'] == end_position_team]

        output_frame = team_row[['club','punten','doelsaldo']].copy()
        output_frame["scenario"] = desc
        output_frame["plaats"] = team_row.index.values[0] +1

        output_list = pd.concat([output_list, output_frame])#output_list.append(output_frame)


    return output_list




def generate_scenarios(matches: List[Tuple], goal_distribution=3):
    result = []
    for score_away in range(0, goal_distribution + 1):
        for score_home in range(0, goal_distribution + 1):
                zzz = {'thuis': [], 'uit': [], 'voor': [], 'tegen': []}
                for home, away in matches:
                    zzz['thuis'].append(home)
                    zzz['uit'].append(away)
                    zzz['voor'].append(score_home)
                    zzz['tegen'].append(score_away)

                #df = pd.DataFrame(zzz)
                result.append(pd.DataFrame(zzz))

    return result




if __name__ == '__main__':
    # sc = generate_scenarios(matches=[('FC Emmen','VVV-Venlo'),
    #                             ('FC Twente', 'Vitesse'),
    #                             ('Fortuna Sittard', 'PEC Zwolle')])

    to_be_executed = [
        {'file': 'scenario-1.csv',
         'desc': 'VVV verliest, Twente verliest, Fortuna wint'},
        {'file': 'scenario-2.csv',
         'desc': 'VVV verliest, Twente verliest, PEC wint'},
        {'file': 'scenario-3.csv',
         'desc': 'VVV verliest, Twente verliest, Fortuna/PEC gelijk'},
        {'file': 'scenario-4.csv',
         'desc': 'VVV verliest, Twente wint, Fortuna wint'},
        {'file': 'scenario-5.csv',
         'desc': 'VVV verliest, Twente wint, PEC wint'},
        {'file': 'scenario-6.csv',
         'desc': 'VVV verliest, Twente wint, Fortuna/PEC gelijk'},
        {'file': 'scenario-7.csv',
         'desc': 'VVV verliest, Twente/Vitesse gelijk, Fortuna wint'},
        {'file': 'scenario-8.csv',
         'desc': 'VVV verliest, Twente/Vitesse gelijk, Pec wint'},
        {'file': 'scenario-9.csv',
         'desc': 'VVV verliest, Twente/Vitesse gelijk, Fortuna/PEC gelijk'},
        {'file': 'scenario-10.csv',
         'desc': 'VVV gelijk, Twente verliest, Fortuna wint'},
        {'file': 'scenario-11.csv',
         'desc': 'VVV gelijk, Twente verliest, PEC wint'},
        {'file': 'scenario-12.csv',
         'desc': 'VVV gelijk, Twente verliest, Fortuna/PEC gelijk'},
        {'file': 'scenario-13.csv',
         'desc': 'VVV gelijk, Twente wint, Fortuna wint'},
        {'file': 'scenario-14.csv',
         'desc': 'VVV gelijk, Twente wint, PEC wint'},
        {'file': 'scenario-15.csv',
         'desc': 'VVV gelijk, Twente wint, Fortuna/PEC gelijk'},
        {'file': 'scenario-16.csv',
         'desc': 'VVV gelijk, Twente/Vitesse gelijk, Fortuna wint'},
        {'file': 'scenario-17.csv',
         'desc': 'VVV gelijk, Twente/Vitesse gelijk, Pec wint'},
        {'file': 'scenario-18.csv',
         'desc': 'VVV gelijk, Twente/Vitesse gelijk, Fortuna/PEC gelijk'},
        {'file': 'scenario-19.csv',
         'desc': 'VVV wint, Twente verliest, Fortuna wint'},
        {'file': 'scenario-20.csv',
         'desc': 'VVV wint, Twente verliest, PEC wint'},
        {'file': 'scenario-21.csv',
         'desc': 'VVV wint, Twente verliest, Fortuna/PEC gelijk'},
        {'file': 'scenario-22.csv',
         'desc': 'VVV wint, Twente wint, Fortuna wint'},
        {'file': 'scenario-23.csv',
         'desc': 'VVV wint, Twente wint, PEC wint'},
        {'file': 'scenario-24.csv',
         'desc': 'VVV wint, Twente wint, Fortuna/PEC gelijk'},
        {'file': 'scenario-25.csv',
         'desc': 'VVV wint, Twente/Vitesse gelijk, Fortuna wint'},
        {'file': 'scenario-26.csv',
         'desc': 'VVV wint, Twente/Vitesse gelijk, Pec wint'},
        {'file': 'scenario-27.csv',
         'desc': 'VVV wint, Twente/Vitesse gelijk, Fortuna/PEC gelijk'},

    ]

    team = 'VVV-Venlo'
    output = execute_scenarios(to_be_executed, team)
    print(output)



    # executed_scenarios = []
    #
    # current_standings = load_current_standings()
    # scenario1 = load_scenario('scenario-1.csv',current_standings['club'])
    # new_standings_sc1 = calculate_new_standings(current_standings, scenario1)
    # executed_scenarios.append({'description':'VVV verliest, Twente verliest, Fortuna wint',
    #                            'data': new_standings_sc1,
    #                            'vvv_position': new_standings_sc1.index[new_standings_sc1['club'] == 'VVV-Venlo'].values[0] + 1})
    #
    #
    # print(new_standings_sc1)
    #
    # current_standings = load_current_standings()
    # scenario2 = load_scenario('scenario-2.csv', current_standings['club'])
    # new_standings_sc2 = calculate_new_standings(current_standings, scenario2)
    # print('VVV verliest, Twente verliest, PEC wint')
    # print(new_standings_sc2)
    #
    # current_standings = load_current_standings()
    # scenario3 = load_scenario('scenario-3.csv', current_standings['club'])
    # new_standings_sc3 = calculate_new_standings(current_standings, scenario3)
    # print('VVV verliest, Twente verliest, Fortuna/PEC spelen gelijk')
    # print(new_standings_sc3)
    #
    # current_standings = load_current_standings()
    # scenario4 = load_scenario('scenario-4.csv', current_standings['club'])
    # new_standings_sc4 = calculate_new_standings(current_standings, scenario4)
    # print('VVV verliest, Twente wint, Fortuna wint')
    # print(new_standings_sc4)
    #
    # current_standings = load_current_standings()
    # scenario5 = load_scenario('scenario-5.csv', current_standings['club'])
    # new_standings_sc5 = calculate_new_standings(current_standings, scenario5)
    # print('VVV verliest, Twente wint, PEC wint')
    # print(new_standings_sc5)
    #
    # current_standings = load_current_standings()
    # scenario6 = load_scenario('scenario-6.csv', current_standings['club'])
    # new_standings_sc6 = calculate_new_standings(current_standings, scenario6)
    # print('VVV verliest, Twente wint, Fortuna/PEC spelen gelijk')
    # print(new_standings_sc6)
    #
    #
    # current_standings = load_current_standings()
    # scenario7 = load_scenario('scenario-7.csv', current_standings['club'])
    # new_standings_sc7 = calculate_new_standings(current_standings, scenario7)
    # print('VVV verliest, Twente speelt gelijk, Fortuna wint')
    # print(new_standings_sc7)
    #
    # current_standings = load_current_standings()
    # scenario8 = load_scenario('scenario-8.csv', current_standings['club'])
    # new_standings_sc8 = calculate_new_standings(current_standings, scenario8)
    # print('VVV verliest, Twente speelt gelijk, PEC wint')
    # print(new_standings_sc8)
    #
    # current_standings = load_current_standings()
    # scenario9 = load_scenario('scenario-9.csv', current_standings['club'])
    # new_standings_sc9 = calculate_new_standings(current_standings, scenario6)
    # print('VVV verliest, Twente speelt gelijk, Fortuna/PEC spelen gelijk')
    # print(new_standings_sc9)



